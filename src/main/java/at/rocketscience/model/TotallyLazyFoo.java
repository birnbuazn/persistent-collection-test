package at.rocketscience.model;

import java.util.Objects;

import com.googlecode.totallylazy.collections.PersistentList;

public final class TotallyLazyFoo {

    private final PersistentList<Bar> bars;

    private TotallyLazyFoo(final PersistentList<Bar> bars) {
        Objects.requireNonNull(bars);
        this.bars = bars;
    }

    public static TotallyLazyFoo create() {
        return new TotallyLazyFoo(PersistentList.constructors.empty(Bar.class));
    }

    public PersistentList<Bar> getBars() {
        return bars;
    }

    public TotallyLazyFoo withBars(PersistentList<Bar> bars) {
        return new TotallyLazyFoo(bars);
    }

    @Override
    public String toString() {
        return "Foo{" +
                "bars=" + bars +
                '}';
    }
}
