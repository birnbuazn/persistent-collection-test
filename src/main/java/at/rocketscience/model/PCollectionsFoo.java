package at.rocketscience.model;

import java.util.Objects;

import org.pcollections.PVector;
import org.pcollections.TreePVector;

public final class PCollectionsFoo {

    private final PVector<Bar> bars;

    private PCollectionsFoo(final PVector<Bar> bars) {
        Objects.requireNonNull(bars);
        this.bars = bars;
    }

    public static PCollectionsFoo create() {
        return new PCollectionsFoo(TreePVector.<Bar>empty());
    }

    public PVector<Bar> getBars() {
        return bars;
    }

    public PCollectionsFoo withBars(PVector<Bar> bars) {
        return new PCollectionsFoo(bars);
    }

    @Override
    public String toString() {
        return "Foo{" +
                "bars=" + bars +
                '}';
    }
}
