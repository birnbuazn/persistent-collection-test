package at.rocketscience;

import org.junit.Test;

import at.rocketscience.model.Bar;
import at.rocketscience.model.TotallyLazyFoo;

public class TotallyLazyFooTest {

    private static final int HUNDRED_THOUSANDS = 100000;

    private Bar mars = Bar.create("1", "Mars");
    private Bar raider = Bar.create("2", "Raider");
    private Bar twix = Bar.create("2", "Twix");

    @Test
    public void testListModification() {
        TotallyLazyFoo version1 = TotallyLazyFoo.create();
        TotallyLazyFoo version2 = version1.withBars(version1.getBars().append(mars).append(raider));
        TotallyLazyFoo version3 = version2.withBars(version2.getBars().delete(raider).append(twix));
        System.out.println(version1);
        System.out.println(version2);
        System.out.println(version3);
    }

    @Test
    public void testPerformance() {
        long start = System.currentTimeMillis();
        TotallyLazyFoo foos = TotallyLazyFoo.create();
        for (int i = 0; i < HUNDRED_THOUSANDS; i++) {
            foos = foos.withBars(foos.getBars().append(Bar.create(String.valueOf(i), "Schokoriegel")));
        }
        long stop = System.currentTimeMillis();
//        System.out.println(foos);
        System.out.println("Execution time:" + (stop - start) + "ms");
    }

}
