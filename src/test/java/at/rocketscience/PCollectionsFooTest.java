package at.rocketscience;

import org.junit.Test;

import at.rocketscience.model.Bar;
import at.rocketscience.model.PCollectionsFoo;

public class PCollectionsFooTest {

    private static final int HUNDRED_THOUSANDS = 100000;

    private Bar mars = Bar.create("1", "Mars");
    private Bar raider = Bar.create("2", "Raider");
    private Bar twix = Bar.create("2", "Twix");

    @Test
    public void testListModification() {
        PCollectionsFoo version1 = PCollectionsFoo.create();
        PCollectionsFoo version2 = version1.withBars(version1.getBars().plus(mars).plus(raider));
        PCollectionsFoo version3 = version2.withBars(version2.getBars().minus(raider).plus(twix));
        System.out.println(version1);
        System.out.println(version2);
        System.out.println(version3);
    }

    @Test
    public void testPerformance() {
        long start = System.currentTimeMillis();
        PCollectionsFoo foos = PCollectionsFoo.create();
        for (int i = 0; i < HUNDRED_THOUSANDS; i++) {
            foos = foos.withBars(foos.getBars().plus(Bar.create(String.valueOf(i), "Schokoriegel")));
        }
        long stop = System.currentTimeMillis();
//        System.out.println(foos);
        System.out.println("Execution time:" + (stop - start) + "ms");
    }


}
